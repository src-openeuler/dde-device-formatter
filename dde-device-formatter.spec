%define specrelease 1%{?dist}
%if 0%{?openeuler}
%define specrelease 1
%endif

Name:           dde-device-formatter
Version:        0.0.1.15
Release:        %{specrelease}
Summary:        A simple graphical interface for creating file system in a block device
License:        GPLv3+
URL:            https://github.com/linuxdeepin/dde-device-formatter
Source0:        %{name}-%{version}.tar.gz

BuildRequires: qt5-devel
BuildRequires: dtkcore-devel
BuildRequires: dtkwidget-devel
BuildRequires: dtkgui-devel
BuildRequires: udisks2-qt5-devel
BuildRequires: deepin-gettext-tools

%description
%{summary}.

%prep
%autosetup

%build
export PATH=%{_qt5_bindir}:$PATH
mkdir build && pushd build
%qmake_qt5 ../ VERSION=%{version} LIB_INSTALL_DIR=%{_libdir} DEFINES+="VERSION=%{version}"
%make_build
popd

%install
%make_install -C build INSTALL_ROOT="%buildroot"


%files
%doc README.md
%license LICENSE
%{_bindir}/%{name}
%{_datadir}/%{name}/*
%{_datadir}/applications/dde-device-formatter.desktop

%changelog
* Wed Mar 29 2023 liweiganga <liweiganga@uniontech.com> - 0.0.1.15-1
- update: update to 0.0.1.15

* Tue Jul 19 2022 konglidong <konglidong@uniontech.com> - 0.0.1.8-1
- update to 0.0.1.8

* Thu Jul 08 2021 weidong <weidong@uniontech.com> - 0.0.1.6-1
- Update 0.0.1.6

* Thu Sep 10 2020 chenbo pan <panchenbo@uniontech.com> - 0.0.1.1-1
- Initial package build
